/**

EmailHooks
EmailHooks allow us to call hooks which should send emails out from packages but can be overriden at the application level

**/

var emailHooks = {} // email hooks key to the function to be evaluated

EmailHooks = {
	
	register: function (codename, hook) {
		emailHooks[codename] = hook;
	},

	call: function (codename, data) {
		var hook = emailHooks[codename];
		if (!hook) return false;

		DefaultEmailType = codename; // set this in case we are instantiating a TemplateEmail
		var _return = hook(data); // true on success, false on failure
		DefaultEmailType = null;

		return _return;
	}

}