DefaultEmailType = null; // a type of initiate all emails with
						 // this is auto set and unset inbetween email hooks (for example)

TemplateEmail = function (to, subject, from) {
	this.to = to;
	this.from = from || HCConfig.emailFrom;
	this.subject = subject;

	this.type = DefaultEmailType;

	this.template = null;
	this.templateProps = null;

	this.content = null;
	this.text = null;
	this.html = null;

	this._cc = null;
	this._bcc = null;
}

TemplateEmail.prototype.bcc = function (_email) {
	this._bcc = this._bcc || [];
	this._bcc.push(_email);
}

TemplateEmail.prototype.cc = function (_email) {
	this._cc = this._cc || [];
	this._cc.push(_email);
}

// Sets the grouping under which we monitor stats about this transactional email (only supported via campaign monitor right now)

TemplateEmail.prototype.setType = function (_type) {
	this.type = _type;
}

// Set React EmailTemplate object

TemplateEmail.prototype.setTemplate = function (_template, _props) {
	this.template = _template;
	this.templateProps = _props;
}

// Set HTML content for the email template directly - will render into our standard email boilerplate template

TemplateEmail.prototype.setContent = function (_content) {
	this.content = _content;
}

// Set a raw string as the content of the email
// String can be text or html, depending on methods called

TemplateEmail.prototype.setText = function (_text) {
	this.text = _text;
}
TemplateEmail.prototype.setHtml = function (_html) {
	this.html = _html;
}

// Load in juice to inline CSS in the emails we are sending

var juice = Npm.require('juice');

TemplateEmail.prototype.send = function () {
	var _text;
	var _html;

	if (this.text) {
		_text = this.text;
	} else if (this.html) {
		_html = this.html;
	} else { // render using a template or boilerplate
		var renderComponent;
		if (this.template) {
			renderComponent = React.createElement(this.template, this.templateProps);
		} else {
			if (EmailBoilerplateTemplate) {
				renderComponent = React.createElement(EmailBoilerplateTemplate, {
					content: this.content
				});
			}
		}

		_html = renderComponent ? ReactDOMServer.renderToStaticMarkup(renderComponent) : this.content;
		
		if (EmailBoilerplateTemplate_RequireCSS) _html = _html.replace("<head></head>",'<head><style type="text/css">'+EmailBoilerplateTemplate_RequireCSS+'</style></head>'); // require the CSS that we know we want as part of the email
		if (EmailBoilerplateTemplate_InlineCSS) _html = juice.inlineContent(_html, EmailBoilerplateTemplate_InlineCSS); // inline the css we want to inline using juice
	}

	_headers = {
		'X-CMail-TrackOpens': true,
		'X-CMail-TrackClicks': true
	}
	
	if (this.type) _headers['X-Cmail-GroupName'] = this.type;

	try {
		Email.send({ // sends via Mandrill
			to: this.to,
			from: this.from,

			cc: this._cc,
			bcc: this._bcc,
			
			subject: this.subject,
			html: _html,
			text: _text,

			headers: _headers
		});
	} catch (err) {
		console.log("Error sending email");
		console.log(err);

		return false;
	}

	return true;
}

// Meteor.startup(function() {
// 	// TEST

// 	//var email = new TemplateEmail("josephros@me.com", "Test", "joe@howcloud.com");
// 	//email.setContent("<h1>Header String</h1><p>Yo this is a test</p>");
// 	//email.setRaw("Testing\n\nYo");
// 	//email.send();
// });