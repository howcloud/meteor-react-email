/**

EmailTemplates
Manages the settings for our email templates
Right now this just assumes one main boilerplate template but a major todo would be to make this abstractable to multiple boilerplates/template types which can wrap the content of a TemplateEmail
Once we abstract the boilerplate just becomes the default wrapping template for email content

**/

EmailBoilerplateTemplate = null;
EmailBoilerplateTemplate_RequireCSS = null;
EmailBoilerplateTemplate_InlineCSS = null;

EmailTemplates = {

	setBoilerplate: function (component) {
		EmailBoilerplateTemplate = component;
	},

	setBoilerplateInlinableCSS: function (css) {
		EmailBoilerplateTemplate_InlineCSS = css;
	},

	setBoilerplateRequireCSS: function (css) {
		EmailBoilerplateTemplate_RequireCSS = css;
	},

}