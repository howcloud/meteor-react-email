Package.describe({
	name: "howcloud:react-email",
	summary: "Exports a TemplateEmail class which can be used to send emails templated with React component which are registered with us. Server side only."
});

Npm.depends({
	"juice": "1.4.2" // for inlining CSS into emails
});

Package.on_use(function (api) {
	
	/* Package Dependencies */

	api.use('underscore');
	api.use('mongo');
	api.use('meteor');
	api.use('email'); // we send through the default meteor system to send emails

	api.use('howcloud:react'); // so we can do the actual rendering, although really any sort of react would do - doesn't have to be our HC version

	/* Add files */

	api.add_files('TemplateEmail.js', ['server']);
	api.add_files('EmailTemplates.js', ['server']);
	api.add_files('EmailHooks.js', ['server']);

	/* Export */

	api.export('TemplateEmail', ['server']);
	api.export('EmailTemplates', ['server']);
	api.export('EmailHooks', ['server']);

});